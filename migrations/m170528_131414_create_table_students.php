<?php

use yii\db\Migration;

class m170528_131414_create_table_students extends Migration
{
    public function up()
    {
		$this->createTable(
           'students',
            [
                'id' => 'pk',
                'name' => 'string',
                'age' => 'string',
                      ],
            'ENGINE=InnoDB'
        );
    }

    public function down()
    {
       //echo "m170521_161426_create_table_customers cannot be reverted.\n";
		$this->dropTable('students');
        //return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
